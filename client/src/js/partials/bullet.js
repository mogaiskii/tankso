var GameObject = require('./game-object').default;
var Enemy = require('./enemy').default;
export default function Bullet(sprite, x, y, width, height) {
  this.obj = new GameObject(sprite, x, y, width, height);
  this.render = this.obj.render;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.update = function(events) {
    
  }
  this.moveTo = function(x, y) {
    this.x = x
    this.obj.x = x
    this.y = y
    this.obj.y = y
  }
  this.render = function(drawer) {
    this.obj.render(drawer);
  }
}