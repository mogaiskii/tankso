export default function Collider() {
  this.objects = []
  this.getCollisions = function(object, dx, dy, fast) {
    if (!fast) { // TODO: пригодится позже
      fast = false
    }
    var pretendents = []
    // console.log('c')
    for (var i = 0; i < this.objects.length; i++) {
      if (Math.abs(object.x - this.objects[i].x) < 200 && Math.abs(object.y - this.objects[i].y) < 200) {
        if (object != this.objects[i]) {
          var pretendent = this.objects[i]
          var pretendentX1 = pretendent.x;
          var pretendentX2 = pretendent.x + pretendent.width;
          var pretendentY1 = pretendent.y;
          var pretendentY2 = pretendent.y + pretendent.height;

          var objectX1 = object.x + dx;
          var objectX2 = object.x + dx + object.width;
          var objectY1 = object.y + dy;
          var objectY2 = object.y + dy + object.height;
          
          var xCollides = false;
          if (objectX1 < pretendentX1) {
            var line = pretendentX2 - objectX1;
            var width1 = pretendent.width;
            var width2 = object.width;
            if (Math.abs(line) < width1 + width2) {
              xCollides = true
            }
          } else {
            var line = objectX2 - pretendentX1;
            var width1 = pretendent.width;
            var width2 = object.width;
            if (Math.abs(line) < width1 + width2) {
              xCollides = true
            }
          }
          var yCollides = false;
          if (objectY1 < pretendentY1) {
            var line = pretendentY2 - objectY1;
            var height1 = pretendent.height;
            var height2 = object.height;
            if (Math.abs(line) < height1 + height2) {
              yCollides = true
            }
          } else {
            var line = objectY2 - pretendentY1;
            var height1 = pretendent.height;
            var height2 = object.height;
            if (Math.abs(line) < height1 + height2) {
              yCollides = true
            }
          }
          if (xCollides && yCollides) {
            pretendents.push(pretendent)
          }
        }
      }
    }
    return pretendents
  }
}