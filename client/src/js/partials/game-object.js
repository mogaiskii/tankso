export default function GameObject(sprite, x, y, width, height, angle) {
  this.solid = false;
  this.image = sprite;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.angle = angle;


  this.render = function(drawer) {
    drawer.drawImage(this.image.image, this.x, this.y, this.width, this.height, this.angle)
  }
}