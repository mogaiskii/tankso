export default function Drawer(canvas) {
  var canvas = canvas;
  var context = canvas.getContext('2d');
  context.lineCap = "round";
  var selfX = 0;
  var selfY = 0;

  function rotateAndDrawImage ( image, angleInRad, x, y, width, height ) {
    context.translate( (x + width/2), (y + height/2) );
    context.rotate( angleInRad );
    context.drawImage( image, -width/2, -height/2, width, height );
    context.rotate( -angleInRad );
    context.translate( -(x + width/2), -(y + height/2) );
  }

  this.drawImage = function(image, x, y, width, height, angleInRad) {
    // console.log('q')
    if (!angleInRad) {
      context.drawImage(image, x, y, width, height);
    } else {
      rotateAndDrawImage(image, angleInRad, x, y, width, height);
    }
  }
  this.clearRect = function(x, y, width, height) {
    context.clearRect(x, y, width, height)
  }
  this.clear = function() {
    context.clearRect(selfX, selfY, canvas.width, canvas.height)
  }
  this.translate = function(dx, dy) {
    context.translate( dx, dy );
    selfX -= dx;
    selfY -= dy;
  }
}