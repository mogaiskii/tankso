export default function Sprite(src) {
  this.image = null;
  this.load = function() {
    return new Promise((resolve, reject) => {
      this.image = new Image();
      this.image.src = src;
      if (this.image.complete) {
        return resolve()
      } else {
        this.image.addEventListener('load', () => {
          resolve()
        })
        this.image.addEventListener('error', (e) => {
          reject(e)
        })
      }
    })
  }
}