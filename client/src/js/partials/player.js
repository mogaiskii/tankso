export default function Player(head, body, _x, _y) {
  this.solid = true;
  this.head = head;
  this.body = body;
  this.width = body.width;
  this.height = body.height;
  this.x = _x;
  this.y = _y;
  this.angle = 0;
  var dx = 0;
  var dy = 0;
  this.dx = 0;
  this.dy = 0;
  this.update = function(events) {
    this.dx = 0;
    this.dy = 0;
    dx = 0;
    dy = 0;
    if (events.updates && events.updates.x) {
      dx = events.updates.x - this.x;
      dy = events.updates.y - this.y;
    }
    if (events.updates && ~events.updates.rotation) {
      this.rotate(events.updates.rotation)
    }
  
    this.moveTo(this.x + dx, this.y + dy);
  }
  this.rotate = function (angleLabel) {
    if (angleLabel == 0) {
      this.angle = 0
    } else if (angleLabel == 1) {
      this.angle = Math.PI / 4
    } else if (angleLabel == 2) {
      this.angle = Math.PI / 2
    } else if (angleLabel == 3) {
      this.angle = Math.PI / 4 + Math.PI/2
    }
    this.body.angle = this.angle
  }
  this.rotateHead = function(angle) {
    this.head.angle = angle
  }
  this.moveTo = function(x, y) {
    this.body.x = x;
    this.body.y = y;
    this.head.x = x;
    this.head.y = y;
    this.x = x;
    this.y = y;
  }
  this.render = function(drawer) {
    drawer.translate(-dx, -dy);
    this.body.render(drawer);
    this.head.render(drawer);
  }
}