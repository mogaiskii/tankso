export default function Enemy(head, body, _x, _y) {
  this.updated = false
  this.solid = true;
  this.hp = 10;
  this.head = head;
  this.body = body;
  this.width = body.width;
  this.height = body.height;
  this.x = _x;
  this.y = _y;
  this.angle = 0;
  this.update = function(events) {

  }
  this.moveTo = function(coords) {
    var cx = coords[0]
    var cy = coords[1]
    this.x = cx
    this.y = cy
    this.body.x = cx
    this.body.y = cy
    this.head.x = cx
    this.head.y = cy
  }
  this.rotate = function (angleLabel) {
    if (angleLabel == 0) {
      this.angle = 0
    } else if (angleLabel == 1) {
      this.angle = Math.PI / 4
    } else if (angleLabel == 2) {
      this.angle = Math.PI / 2
    } else if (angleLabel == 3) {
      this.angle = Math.PI / 4 + Math.PI/2
    }
    this.body.angle = this.angle
  }
  this.rotateHead = function(angleLabel) {
    this.head.angle = angleLabel * Math.PI/16
  }
  this.render = function(drawer) {
    this.body.render(drawer);
    this.head.render(drawer);
  }
}