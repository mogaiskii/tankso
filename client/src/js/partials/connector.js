export default function Connector() {
  var sock;
  this.id = -1;
  this.load = function(url) {
    sock = new WebSocket(url);
    var conn = new Promise((resolve, reject) => {
      sock.onopen = () => {
        return resolve()
      };
    })
    var map = new Promise((resolve, reject) => {
      sock.onmessage = (e) => this.update(e, resolve)
    })
    sock.onclose = () => {
      sock = null
      throw new Error()
    }
    return Promise.all([conn, map])
  }
  this.info = {}
  this.updated = false;
  var mapLoaded = false;
  // this.send = function(x, y, bulletX, bulletY) {
  //   // var bignum = ~~x + (~~y)*4096 + (~~(bulletX || 0))*4096*4096 + (~~(bulletY || 0))*4096*4096*4096;
  //   // //
  //   if (!sock) return;
  //   // sock.send(JSON.stringify([x, y, bulletX, bulletY]))
  // }
  this.send = function(k) {
    if (!sock) return;
    sock.send(k)
  }
  this.update = function(e, r) {
    if (!mapLoaded) {
      var data = JSON.parse(e.data)
      var map = data[1]
      this.id = data[0]
      mapLoaded = true;
      return r(map)
    } else {
      this.info = JSON.parse(e.data)
      this.updated = true
    }
  }
}