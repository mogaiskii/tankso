

var prefix = '/public/'
var protocol = 'ws://'
if (window.location.protocol == 'https:') {
  protocol = 'wss://'
}
var URL = protocol + window.location.host + '/ws_conn'
// var URL = 'wss://tankso.herokuapp.com/ws_conn'
// var URL = 'ws://seikolocal.com/ws_conn'
// var URL = 'ws://localhost:8888/ws_conn'
// var URL = 'ws://localhost:8080/'
var Sprite = require('./partials/sprite').default;
var Drawer = require('./partials/drawer').default;
var GameObject = require('./partials/game-object').default;
var Enemy = require('./partials/enemy').default;
var Player = require('./partials/player').default;
var Collider = require('./partials/collider').default;
var Connector = require('./partials/connector').default;

document.addEventListener("DOMContentLoaded", function(event) {
  var body = new Sprite(prefix + 'body.png');
  var enemyBody = new Sprite(prefix + 'body-enemy.png');
  var head = new Sprite(prefix + 'head.png');
  var wall = new Sprite(prefix + 'wall.png');
  var bullet = new Sprite(prefix + 'bullet.png');
  var connector = new Connector();
  var map = null;


  // preload
  return new Promise((resolve, reject) => {
    return resolve(Promise.all([connector.load(URL), body.load(), enemyBody.load(), head.load(), wall.load(), bullet.load()]))
  })
    .then((ans) => {
      map = ans[0][1]
      // game state
      var canvas = document.getElementById('canvas');
      var canvasWidth = 800;
      var canvasHeight = 600;
      canvas.width = canvasWidth;
      canvas.height = canvasHeight;
      var cellWidth = 128;
      var cellHeight = 128;
      var drawer = new Drawer(canvas);
      var collider = new Collider();
      
      var events = {
        keys: {},
        delta: 0,
        updates: {},
        mouse: {
          x: null,
          y: null,
          leftButton: null
        }
      }

      // event handlers
      document.addEventListener('keydown', function(e) {
        events.keys[e.keyCode] = true;
      })
      document.addEventListener('keyup', function(e) {
        events.keys[e.keyCode] = false;
      })
      document.addEventListener('mousemove', function(e) {
        events.mouse.x = e.pageX - canvas.offsetLeft;
        events.mouse.y = e.pageY - canvas.offsetTop;
      });
      document.addEventListener('mousedown', function(e) {
        e.preventDefault();
        events.mouse.leftButton = true;
      });
      document.addEventListener('mouseup', function(e) {
        e.preventDefault();
        events.mouse.leftButton = false;
      });

      // map read
      var walls = [];
      var c_y = 0;
      for (var i = 0; i < map.length; i++) {
        var c_x = 0;
        for (var j = 0; j < map.length; j++) {
          var c = map[i][j]
          if (c == 1) {
            // wall
            var wallObject = new GameObject(wall, c_x, c_y, cellWidth, cellHeight);
            wallObject.solid = true;
            walls.push(wallObject);
            collider.objects.push(wallObject)
          }
          c_x += cellWidth;
        }
        c_y += cellHeight;
      }
      var playerBody = new GameObject(
        body,
        0,
        0,
        cellWidth/2,
        cellHeight/2
      );
      var playerHead = new GameObject(
        head,
        0,
        0,
        cellWidth/2,
        cellHeight/2
      );
      var player = new Player(playerHead, playerBody, 0, 0, collider, bullet);
      collider.objects.push(player);
      drawer.translate(cellWidth*3 - cellWidth/4,cellHeight*2 - cellWidth/4);
      var centerx = cellWidth*3 - cellWidth/4 + cellWidth/4
      var centery = cellHeight*2 - cellWidth/4 + cellWidth/4
      window.ce=[centerx, centery]

      // loop
      var enemyHash = {}
      var lastTime = new Date();
      function mainLoop() {
        events.updates = null
        // console.log('1')
        var newTime = new Date();
        events.delta = (newTime - lastTime)/1000;
        lastTime = newTime;
        let bullets = []
        
        if (connector.updated) {
          connector.updated = false;
          for (var upd of connector.info) {
            if (connector.id == upd[0]) {
              events.updates = {
                x: upd[1],
                y: upd[2],
                rotation: upd[4]
              }
              bullets = bullets.concat(upd[3])
              console.log(upd[6])
            } else {
              var enemy = null
              if (enemyHash[upd[0]]) {
                enemy = enemyHash[upd[0]]
                enemy.moveTo([upd[1], upd[2]])
                bullets = bullets.concat(upd[3])
                enemy.rotate(upd[4])
                enemy.rotateHead(upd[5])
              } else {
                var eBody = new GameObject(
                  enemyBody,
                  upd[1],
                  upd[2],
                  cellWidth/2,
                  cellHeight/2
                );
                var eHead = new GameObject(
                  head,
                  upd[1],
                  upd[2],
                  cellWidth/2,
                  cellHeight/2
                );
                enemy = new Enemy(eHead, eBody, upd[1], upd[2], collider, bullet)
                enemyHash[upd[0]] = enemy
              }
              enemy.updated = true
            }
          }
          var ids = Object.keys(enemyHash)
          for (var i = 0; i < ids.length; i++) {
            if (enemyHash[ids[i]].updated) {
              enemyHash[ids[i]].updated = false
            } else {
              delete enemyHash[ids[i]]
            }
          }
        }
        player.update(events);
        var angle = Math.atan2(centerx - events.mouse.x, events.mouse.y - centery) + Math.PI;
        angle = Math.round(angle / (Math.PI/16))
        window.angle = angle
        player.rotateHead(angle * Math.PI/16)
        var key1 = '_'
        var key2 = '_'
        if (events.keys[65]) {
          key1 = 'a'
        }
        if (events.keys[87]) {
          key2 = 'w'
        }
        if (events.keys[68]) {
          key1 = 'd'
        }
        if (events.keys[83]) {
          key2 = 's'
        }
        var action = '0'
        if (events.mouse.leftButton) {
          action = '1'
        }
        connector.send(key1+key2+String.fromCharCode(48+angle)+action)


        drawer.clear();
        for (var i = 0; i < walls.length; i++) {
          walls[i].render(drawer)
        }
        player.render(drawer);
        for (var eid in enemyHash) {
          enemyHash[eid].render(drawer)
        }
        for (var i = 0; i < Math.floor(bullets.length); i++) {
          drawer.drawImage(bullet.image, bullets[i][0], bullets[i][1], 32, 32)
        }
      }
      window.shouldStop = false
      function mainLoopWrapper() {
        if (window.shouldStop) {
          return
        }
        mainLoop()
        requestAnimationFrame(mainLoopWrapper)
      }
      requestAnimationFrame(mainLoopWrapper)
    })
});
