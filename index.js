var express = require('express');
var http = require('http');
var path = require('path');
const WebSocket = require('ws');
const GameServer = require('./server/gameserver');

var app = express();
var server = http.Server(app);
var port = process.env.PORT || 8888
app.set('port', port);
app.use(express.static(__dirname + '/build'));
// Routing
app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname, 'build/index.html'));
});

const wss = new WebSocket.Server({ server: server, path: '/ws_conn' })

const gs = new GameServer(4, 180)
gs.configure(wss)

// Starts the server.
server.listen(port, function() {
  console.log(`Starting server on port ${port}`);
});