const GameMap = require('./gamemap');
const Player = require('./player');
const Collider = require('./collider');

module.exports = function GameServer(maxCount, playerSpeed) {
  this.ids = []
  for (let i=0; i<maxCount; i++) {
    this.ids.push(i+1)
  }
  this.players = []
  this.speed = playerSpeed;
  this.loop = null
  const gamemap = new GameMap(128, 128)
  const collider = new Collider(gamemap)
  function restartPlayer(player) {
    const spawn = gamemap.getSpawn()
    player.x = spawn[0]
    player.y = spawn[1]
    player.hp = 10
  }
  this.configure = function(websocket) {
    gamemap.generate()
    gamemap.scanSpawns()
    websocket.on('connection', (ws) => {
      if (this.ids.length <= 0) {
        return ws.close()
      }
      const id = this.ids.shift()
      console.log(`#${id} connected`)
      const spawn = gamemap.getSpawn()
      const p = new Player(ws, id, spawn[0], spawn[1], ~~128/2, ~~128/2, this.speed, collider, restartPlayer)
      this.players.push(p)
      p.init(gamemap)
      collider.addObject(p)

      ws.on('message', (message) => {
        p.handle(message)
      })

      ws.on('close', () => {
        console.log(`#${id} disconnected`)
        let ind = -1
        for (let i = 0; i < this.players.length; i++) {
          if (this.players[i].id == p.id) {
            ind = i;
            break;
          }
        }
        if (~ind) {
          this.ids.push(p.id)
          collider.removeObject(p)
          this.players.splice(ind, 1)
        }
      })
    })

    this.loop = setInterval(() => {
      let updates = []
      for (let p of this.players) {
        if (p.connection.readyState === 1) {
          p.update()
          updates.push(p.serialize())
        }
      }
      const strUpdates = JSON.stringify(updates)
      for (let p of this.players) {
        if (p.connection.readyState === 1) {
          p.sync(strUpdates)
        }
      }
    }, 50)
  }

  this.requestPlayerCoords = function(id) {
    for (var i = 0; i < this.players.length; i++) {
      if (players[i].id == id) {
        return [players[i].x, players[i].y, players[i].x+64, players[i].y+64]
      }
    }
  }
}