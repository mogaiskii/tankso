const Bullet = require('./bullet')

module.exports = function Player(connection, id, x, y, _width, _height, speed, collider, onDeath) {
  this.connection = connection;
  this.id = id;
  this.x = x;
  this.y = y;
  this.width = _width;
  this.height = _height;
  this.direction = 0;
  this.headDirection = 0;
  this.speed = speed;
  this.collider = collider;
  this.bullets = [];
  this.hp = 10;
  var lastShoot = 0;
  var onDeath = onDeath

  this.init = function(gamemap) {
    this.connection.send(JSON.stringify([this.id, gamemap.map]))
  }

  this.damage = function(dmg) {
    this.hp -= dmg
    if (this.hp < 0) {
      onDeath(this)
    }
  }

  var lastupdate = new Date() - 0
  this.handle = function(message) {
    const tick = new Date () - 0
    var delta = tick - lastupdate
    var dx = 0
    var dy = 0
    if (message[0] == 'a') {
      dx -= delta / 1000 * this.speed
    } else if (message[0] == 'd') {
      dx += delta / 1000 * this.speed
    }
    if (message[1] == 'w') {
      dy -= delta / 1000 * this.speed
    } else if (message[1] == 's') {
      dy += delta / 1000 * this.speed
    }
    if (dx && dy) {
      dx *= 0.77;
      dy *= 0.77;
    }
    const coords = this.collider.safeMove(this.x, this.y, this.width, this.height, dx, dy)
    this.x = coords[0];
    this.y = coords[1];
    // var direction = 0
    // 0 - up/down
    // 1 - NE/SW
    // 2 - left/right
    // 3 - NW/SE
    if (message[0] == '_' && message[1] == '_') {
      // nothing
    } else if (message[1] == '_') {
      this.direction = 2
    } else if (Math.sign(dx) == Math.sign(dy)) {
      this.direction = 3
    } else if (Math.sign(dx) == -Math.sign(dy)) {
      this.direction = 1
    } else {
      this.direction = 0
    }
    this.headDirection = (message[2] || '0').charCodeAt() - 48
    if (message[3] == '1') {
      if (tick - lastShoot > 700) {
        this.bullets.push(
          new Bullet(
            this.x +this.width/4,
            this.y +this.height/4,
            this.headDirection,
            this.width / 2,
            this.height / 2,
            this.speed * 3,
            this.collider,
            this
          )
        )
        lastShoot = tick
      }
    }
    lastupdate = tick
  }

  this.update = function() {
    if (this.bullets.length) {
      let forDelete = []
      for (let i = 0; i < this.bullets.length; i++) {
        bullet = this.bullets[i]
        bullet.update()
        if (bullet.speed == 0) {
          forDelete.push(i)
        } else if (bullet.x > 2500 || bullet.y > 2500) {
          forDelete.push(i)
        }
      }
      for (let i = forDelete.length - 1; i >= 0; i--) {
        this.bullets.splice(i, 1)
      }
    }
  }

  this.serialize = function() {
    let bulletX = 0
    let bulletY = 0
    if (this.bullets.length) {
      bulletX = this.bullets[0].x
      bulletY = this.bullets[0].y
    }
    bullets = []
    for (const bullet of this.bullets) {
      bullets.push([bullet.x, bullet.y])
    }
    return [this.id, this.x, this.y, bullets, this.direction, this.headDirection, this.hp]
  }

  this.sync = function(strUpdates) {
    this.connection.send(strUpdates)
  }
}