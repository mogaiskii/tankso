module.exports = function Collider(gamemap, requestPlayerCoords) {
  this.gamemap = gamemap
  this.objects = []
  this.requestPlayerCoords = requestPlayerCoords

  this.addObject = function(obj) {
    this.objects.push(obj)
  }
  this.removeObject = function(obj) {
    const ind = this.objects.findIndex((item) => item === obj)
    if (~ind) {
      this.objects.splice(ind, 1)
    }
  }

  this.isSafeMove = function(x, y, pW, pH, dx, dy) {
    const coords = this.safeMove(x, y, pW, pH, dx, dy)
    if (coords[0] == x+dx && coords[1] == y+dy) {
      return true
    }
    return false
  }

  this.getCollisions = function(x, y, pW, pH, dx, dy) {
    const moore = this.gamemap.getMoore(x, y)
    const local = this.gamemap.toLocal(x, y)
    const x1 = x+dx
    const x2 = x+dx+pW
    const y1 = y+dy
    const y2 = y+dy+pH
    const collisions = []
    for (let cY = -1; cY <= 1; cY++) {
      for (let cX = -1; cX <= 1; cX++) {
        if (cY == 0 && cX == 0) continue;
        let wallX = 0
        let wallX2 = 0
        let wallY = 0
        let wallY2 = 0
        if (moore[cY+1][cX+1] == -1 || moore[cY+1][cX+1] == 1) {
          wallX = (local[0] + cX) * this.gamemap.cWidth
          wallX2 = (local[0] + cX + 1) * this.gamemap.cWidth
          wallY = (local[1] + cY) * this.gamemap.cHeight
          wallY2 = (local[1] + cY + 1) * this.gamemap.cHeight
        } else if (moore[cY+1][cX+1]  > 128) {
          const coords = this.requestPlayerCoords(moore[cY+1][cX+1] - 128)
          wallX = coords[0]
          wallX2 = coords[1]
          wallY = coords[2]
          wallY2 = coords[3]
        } else {
          wallX = (local[0] + cX) * this.gamemap.cWidth
          wallX2 = (local[0] + cX + 1) * this.gamemap.cWidth
          wallY = (local[1] + cY) * this.gamemap.cHeight
          wallY2 = (local[1] + cY + 1) * this.gamemap.cHeight
        }

        if (checkCollision(x1, x2, y1, y2, wallX, wallX2, wallY, wallY2)) {
          collisions.push(moore[cY+1][cX+1])
        }
      }
    }
    for (const obj of this.objects) {
      const objX2 = obj.x + obj.width
      const objY2 = obj.y + obj.height
      if (checkCollision(x1, x2, y1, y2, obj.x, objX2, obj.y, objY2)) {
        collisions.push(obj)
      }
    }
    
    return collisions
  }
  function checkCollision(p1x1, p1x2, p1y1, p1y2, p2x1, p2x2, p2y1, p2y2) {
    const sumX = (p1x2 - p1x1) + (p2x2 - p2x1)
    let mostX = 0
    if (p1x1 < p2x1) {
      mostX = p2x2 - p1x1
    } else if (p1x2 < p2x2) {
      mostX = (p1x2 - p1x1)
    } else {
      mostX = p1x2 - p2x1
    }

    const sumY = (p1y2 - p1y1) + (p2y2 - p2y1)
    let mostY = 0
    if (p1y1 < p2y1) {
      mostY = p2y2 - p1y1
    } else if (p1y2 < p2y2) {
      mostY = (p1y2 - p1y1)
    } else {
      mostY = p1y2 - p2y1
    }
    if (sumY > Math.abs(mostY) && sumX > Math.abs(mostX)) {
      return true
    }
    return false
  }

  this.safeMove = function(x, y, pW, pH, dx, dy) {
    const moore = this.gamemap.getMoore(x, y)
    const local = this.gamemap.toLocal(x, y)
    const x1 = x+dx
    const x2 = x+dx+pW
    const y1 = y+dy
    const y2 = y+dy+pH
    
    for (let cY = -1; cY <= 1; cY++) {
      for (let cX = -1; cX <= 1; cX++) {
        if (cY == 0 && cX == 0) continue;
        if (moore[cY+1][cX+1] == -1 || moore[cY+1][cX+1] == 1) {
          const wallX = (local[0] + cX) * this.gamemap.cWidth
          const wallX2 = (local[0] + cX + 1) * this.gamemap.cWidth
          const wallY = (local[1] + cY) * this.gamemap.cHeight
          const wallY2 = (local[1] + cY + 1) * this.gamemap.cHeight

          const sumX = pW + (wallX2 - wallX)
          let mostX = 0
          if (x1 < wallX) {
            mostX = wallX2 - x1
          } else if (x2 < wallX2) {
            mostX = pW
          } else {
            mostX = x2 - wallX
          }

          const sumY = pH + (wallY2 - wallY)
          let mostY = 0
          if (y1 < wallY) {
            mostY = wallY2 - y1
          } else if (y2 < wallY2) {
            mostY = pH
          } else {
            mostY = y2 - wallY
          }
          if (sumY > Math.abs(mostY)) {
            if (sumX > Math.abs(mostX)) {
              dx = 0
              dy = 0
            }
          }
        }
      }
    }
    
    return [x+dx, y+dy]
  }
}