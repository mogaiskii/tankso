const Player = require('./player');

module.exports = function Bullet(x, y, direction, _width, _height, speed, collider, owner) {
  this.x = x
  this.y = y
  this.direction = direction
  this.width = _width
  this.height = _height
  this.speed = speed
  this.collider = collider
  this.owner = owner

  let lastUpdate = new Date() - 0
  this.update = function(){
    if (!this.speed) {
      return
    }
    const delta = new Date() - lastUpdate
    let dx = Math.sin(this.direction*(Math.PI/16)) * delta / 1000 * this.speed
    let dy = -Math.cos(this.direction*(Math.PI/16)) * delta / 1000 * this.speed
    const collisions = this.collider.getCollisions(this.x, this.y, this.width, this.height, dx, dy)
    // console.log(collisions.length)
    const safeMove = this.collider.safeMove(this.x, this.y, this.width, this.height, dx, dy)
    let unSafe = safeMove[0] != this.x + dx || safeMove[1] != this.y + dy
    for (const collision of collisions) {
      // console.log(collision instanceof this.owner)
      if (collision != this.owner && Object.getPrototypeOf(this.owner) == Object.getPrototypeOf(collision)) {
        collision.damage(1)
        unSafe = true;
        break;
      }
    }
    if (unSafe) {
      this.x = 0
      this.speed = 0
      this.y = 0
      dx = 0
      dy = 0
    }
    this.x += dx
    this.y += dy
    lastUpdate = new Date() - 0
  }
}