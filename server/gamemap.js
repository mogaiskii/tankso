module.exports = function GameMap(ceilWidth, ceilHeight) {
  this.cWidth = ceilWidth
  this.cHeight = ceilHeight
  this.map = []
  this.spawns = []
  this.generate = function() {
    this.map = [
      [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [1, 2, 3, 0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 2, 1],
      [1, 4, 5, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
      [1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 1],
      [1, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1],
      [1, 0, 1, 0, 2, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 2, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 2, 1],
      [1, 0, 0, 0, 0, 0, 1, 0, 0, 2, 1, 0, 0, 1, 0, 1],
      [1, 0, 0, 0, 2, 0, 1, 0, 0, 0, 1, 2, 0, 1, 0, 1],
      [1, 2, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1],
      [1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 2, 1],
      [1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, 1],
      [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ]
  }

  this.scanSpawns = function() {
    this.spawns = []
    for (let cY = 0; cY < this.map.length; cY++) {
      if (this.map[cY] && this.map[cY].length && this.map[cY].length > 0) {
        for (let cX = 0; cX < this.map[cY].length; cX++) {
          if (this.map[cY][cX] === 2) {
            this.spawns.push([this.cWidth * cX-0, this.cHeight * cY-0])
          }
        }
      }
    }
  }
  this.getSpawn = function() {
    // get free spawns
    return this.spawns[~~(Math.random() * this.spawns.length)]
  }

  this.toLocal = function(x, y) {
    const localX = ~~(x / this.cWidth)
    const localY = ~~(y / this.cHeight)
    return [localX, localY]
  }

  this.requestAt = function(x, y) {
    const [localX, localY] = this.toLocal(x, y)
    return this.requestAtLocal(localX, localY)
  }

  this.requestAtLocal = function(localX, localY) {
    if (localX < 0 || localY < 0 || localY >= this.map.length || localX >= this.map[0].length) {
      return -1
    }
    return this.map[localY][localX]
  }

  this.getMoore = function(px, py) {
    const [localX, localY] = this.toLocal(px, py)

    const moore = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
    ]
    for (let y = -1; y <= 1; y++){
      for (let x = -1; x <= 1; x++) {
        moore[y+1][x+1] = this.requestAtLocal(localX+x, localY+y)
      }
    }
    return moore
  }
}